var log4js = require("log4js");
var path = require('path');
var config = require('../config/constants');

log4js.configure({
    levels: {
        OFF: { value: Number.MAX_VALUE-1, colour: 'white' },
        AUDIT: { value: Number.MAX_VALUE, colour: 'yellow' }
    },
    // appenders: { file: { type: 'file', filename: '../logs/' + path.parse(process.cwd()).base, maxLogSize: config.logFileMaxSize, compress: true}},
    //  // categories: { default: { appenders: ['out'], level: 'AUDIT' } },
    // categories: { default: { appenders: ['file'], level: 'info' } }
    appenders: { out: { type: 'stdout' }, file: { type: 'file', filename: '../logs/' + path.parse(process.cwd()).base + '-' + process.env.pm_id + ".log" }},
    categories: { default: { appenders: ['out'], level: 'AUDIT' } },
    categories: { default: { appenders: ['out', 'file'], level: 'info' } }
    //pm2: true,
    //pm2InstanceVar: 'INSTANCE_ID'

});


function getConfig(logPath, maxLogSize, numberOfBackUps, module) {
    return {
        "appenders": [
            {
                "type": "jsonfileappender",
                "filename": logPath,
                "maxLogSize": maxLogSize,
                "backups": numberOfBackUps,
                "category": [module, module + "-controller", "swagger-mongoose-crud"],
                "level": "AUDIT,WARN,ERROR"
            },
            {
                "type": "console",
                "category": [module, module + "-controller", "swagger-mongoose-crud"],
                "level": "AUDIT,WARN,ERROR"
            }
        ]
    }
}


module.exports.getConfig = getConfig;
module.exports.getLogger = log4js;