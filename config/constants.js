module.exports = {
	publicUrls: [
		'/user/v1/swagger',
		'/user/v1/authentication/login',
		'/user/v1/password/validateUserName',
		'/user/v1/password/securityVerification',
		'/user/v1/password/reset',
		'/user/v1/heartBeat',
		'/user/v1/otp/validate',
		'/user/v1/otp/resend',
		// without user
		'/v1/swagger',
		'/v1/authentication/login',
		'/v1/password/validateUserName',
		'/v1/password/securityVerification',
		'/v1/password/reset',
		'/v1/heartBeat',
		'/v1/otp/validate',
		'/v1/otp/resend'
	],
	statusCodes: {
		SUCCESS: {
			statusCode: 100,
			status: 'SUCCESS'
		},
		SERVER_ERROR: {
			statusCode: 101,
			status: "INTERNAL_SERVER_ERROR"
		},
		DB_ERROR: {
			statusCode: 102,
			status: "DATABASE_ERROR"
		},
		DUPLICATE: {
			statusCode: 103,
			status: "DUPLICATE"
		},
		BAD_REQUEST: {
			statusCode: 104,
			status: "BAD_REQUEST"
		},
		No_data_found: {
			statusCode: 404,
			status: "No_data_found"
		},
		Something_went_wrong: {
			statusCode: 400,
			status: "Something_went_wrong"
		},
		UNAUTHORIZED: {
			statusCode: 2605,
			status: "Unauthorized"
		},
	},
	logFileMaxSize: 10485760
}