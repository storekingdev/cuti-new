var counter = require("./counter");
var logger = require("./logger");
var request = require("./Request");
var mongoose = require("./mongoose");
var app = require('./app');
var constants = require('./config/constants');
var logMiddleware = require("./logMiddleware");
var diffChecker = require("./diffChecker");
var masterName = null;

function init(name) {
    masterName = name;
    request.init(masterName);
}

module.exports = {
    init: init,
    counter: counter,
    logger: logger,
    request: request,
    app: app,
    constants: constants,
    logMiddleware: logMiddleware,
    diffChecker: diffChecker,
    mongoose: mongoose
};