const mongoose = require("mongoose");
const connection = mongoose.connection;
const statusCodes = require('../config/constants').statusCodes
const publicUrl = require("../config/constants").publicUrls;


var e = {};

e.findOne = function (collectionName, filter, select) {
    return new Promise((resolve, reject) => {
        connection.db.collection(collectionName, function (err, commonModel) {
            if (err) {
                reject({ message: statusCodes.DB_ERROR.status, error: err });
            } else {
                commonModel.findOne(filter, select)
                    .then((data) => {
                        resolve(data);
                    })
                    .catch(err => {
                        reject({ message: statusCodes.SERVER_ERROR.status, error: err });
                    });
            }
        })
    })
}

e.find = function (collectionName, filter, select) {
    return new Promise((resolve, reject) => {
        connection.db.collection(collectionName, function (err, findModel) {
            if (err) {
                reject({ message: statusCodes.DB_ERROR.status, error: err });
            } else {
                findModel.find(filter, select).toArray()
                    .then((data) => {
                        resolve(data);
                    })
                    .catch(err => {
                        reject({ message: statusCodes.SERVER_ERROR.status, error: err });
                    });
            }
        })
    })
}

e.create = function (collectionName, payload) {
    return new Promise((resolve, reject) => {
        connection.db.collection(collectionName, function (err, insertModel) {
            if (err) {
                reject({ message: statusCodes.DB_ERROR.status, error: err });
            } else {
                insertModel.insert(payload)
                    .then((data) => {
                        resolve(data);
                    })
                    .catch(err => {
                        reject({ message: statusCodes.SERVER_ERROR.status, error: err });
                    });
            }
        })
    })
}

e.update = function (collectionName, filter, payload) {
    return new Promise((resolve, reject) => {
        connection.db.collection(collectionName, function (err, updateModel) {
            if (err) {
                reject({ message: statusCodes.DB_ERROR.status, error: err });
            } else {
                updateModel.update(filter, payload)
                    .then((data) => {
                        resolve(data);
                    })
                    .catch(err => {
                        reject({ message: statusCodes.SERVER_ERROR.status, error: err });
                    });
            }
        })
    })
}


module.exports = e;