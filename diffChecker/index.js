exports.diffCheckerFunc = (obj1, obj2, isDifferenceDetailsRequired) => {
    return new Promise((resolve, reject) => {
        var diff = require('deep-diff').diff;
        var differences = diff(obj1, obj2);
        if (isDifferenceDetailsRequired) {
            resolve(differences);
        } else {
            resolve(differences.map(diffElem => {
                return diffElem.path;
            }))
        }
    })
}

module.exports = exports;