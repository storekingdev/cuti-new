var child_process = require('child_process');
var exports = {};


exports.generateAPIDocs = function(outPath, jarFile, yamlFile, outFormat){
    //var cmd = `cd ./docs && java -jar ../../swagger-codegen-cli-2.2.1.jar generate -i ../api/swagger/swagger.yaml -l html2`
    var cmd = `cd ${outPath} && java -jar ${jarFile} generate -i ${yamlFile} -l ${outFormat}`
    var child = child_process.exec(cmd, []);
    var resp = "";

    child.stdout.on('data', function (buffer) { resp += buffer.toString() });
    child.stdout.on('end', function() {
        // console.log(resp);
     });
}

module.exports = exports;