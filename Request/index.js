var http = require("http");
var puttu = require("puttu-redis");
var masterName = null;
const mongoose = require("mongoose");
const async = require('async');
const crypto = require('crypto');
const moment = require('moment');
const path = require('path');
const jwtDecode = require('jwt-decode');
const statusCode = require('../config/constants').statusCodes
const publicUrl = require("../config/constants").publicUrls;

var e = {};

e.init = (_masterName) => {
    masterName = _masterName;
};

e.getOptions = (url, method, path, magicKey) => {
    var options = {};
    path = url.split("/");
    options.hostname = url.split("//")[1].split(":")[0];
    options.port = url.split(":")[2].split("/")[0];
    options.path = "/" + path.splice(3, path.length - 3).join("/");
    options.method = method;
    options.headers = {};
    options.headers["content-type"] = "application/json";
    options.headers["magicKey"] = magicKey ? magicKey : null;
    return options;
};

e.getUrlandMagicKey = (masterName, retries) => {
    if (!retries) /* then */ retries = Date.now();
    return puttu.get(masterName).then(url => {
        return puttu.getMagicKey(masterName).then(magicKey => {
            var options = {};
            var path = url.split("/");
            options.hostname = url.split("//")[1].split(":")[0];
            options.port = url.split(":")[2].split("/")[0];
            options.path = "/" + path.splice(3, path.length - 3).join("/");
            options.method = "GET";
            options.headers = {};
            options.headers["masterName"] = masterName;
            options.headers["content-type"] = "application/json";
            options.headers["magicKey"] = magicKey ? magicKey : null;
            return getSourceHeader(masterName).then(
                _headerData => {
                    options.headers["sourceMagicKey"] = _headerData;
                    return new Promise(resolve => resolve(options));
                }
            );
        }, err => console.error("error from prehooks internal", err));
    }, () => Date.now() - retries < 500 ? e.getUrlandMagicKey(masterName, retries) : new Promise((resolve, reject) => reject(new Error(masterName + " Service down"))));
};

e.validateSource = (masterName, key) => {
    return puttu.get(masterName).then(() => {
        return puttu.getMagicKey(masterName).then(
            _magicKey => new Promise((resolve, reject) => {
                _magicKey == key ? resolve() : reject();
            })
        );
    });
};

function getSourceHeader(masterName, retries) {
    if (!retries) /* then */ retries = Date.now();
    return puttu.get(masterName).then(() => {
        return puttu.getMagicKey(masterName).then(
            _magicKey => new Promise(resolve => resolve(masterName + "#" + _magicKey)),
            err => console.error("error from prehooks internal", err));
    }, () => Date.now() - retries < 500 ? getSourceHeader(masterName, retries) : new Promise((resolve, reject) => reject(new Error(masterName + " Service down"))));
}

e.checkIfExists = (masterName, id) => {
    var masterOptions = null;
    return new Promise((resolve, reject) => {
        e.getUrlandMagicKey(masterName)
            .then(options => {
                options.path += "/" + id;
                masterOptions = options;
            }, err => reject(err))
            .then(() => getSourceHeader(masterName))
            .then(_hostHeader => {
                masterOptions.headers["sourceMagicKey"] = _hostHeader;
                http.request(masterOptions, response => response.statusCode === 200 ? resolve() : reject(new Error("Invalid " + masterName))).end();
            });
    });
};

e.getElement = (masterName, id, select) => {
    select = select ? "?select=" + select : "";
    var masterOptions = null;
    return e.getUrlandMagicKey(masterName)
        .then(options => {
            options.path += "/" + id + select;
            masterOptions = options;
        })
        .then(() => getSourceHeader(masterName))
        .then(_hostHeader => {
            masterOptions.headers["sourceMagicKey"] = _hostHeader;
            return new Promise((resolve, reject) => {
                http.request(masterOptions, response => {
                    if (response.statusCode != 200) {
                        reject(new Error(masterName + " return with statusCode " + response.statusCode));
                    } else {
                        var data = "";
                        response.on("data", _data => data += _data.toString());
                        response.on("end", () => resolve(JSON.parse(data)));
                    }
                }).end();
            });
        });
};


e.authenticate = (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    let urlKey = req.url.split("/")
    let index;
    index = urlKey.indexOf("swagger");
    if (index === -1) {
        index = urlKey.indexOf("heartBeat");
    }
    if (req.headers.magicKey || req.headers.magickey || req.user || req.method.toLowerCase() === 'options') {
        next();
    } else if (index !== -1) {
        next();
    } else if (req.headers["authorization"]) {
        //has authorization header

        e.makeXCall(req.headers, 'usermgmt', "/v1/authentication/validateToken?api=" + encodeURI(req.url)).then((data) => {
            if (data.isPublic) {
                next();
            } else if (data.user) {
                let user = data.user;
                let token = req.headers["authorization"].split(" ")[1];
                req.session = {
                    user: user,
                    token: token
                };
                next();
            } else {
                res.status(401).json({
                    code: "Unauthorized",
                    message: "Invalid session"
                });
            }
        }).catch((err) => {
            if (err.code === 'Unauthorized' && err.statusCode && err.statusCode === 408) {
                res.status(err.statusCode).json({
                    code: "Unauthorized",
                    message: "Invalid session"
                });
            } else if (err.code === 'Unauthorized') {
                res.status(401).json({
                    code: "Unauthorized",
                    message: "Invalid session"
                });
            }
            else {
                console.log(err);
                res.status(500).json({
                    message: "Server error"
                });
            }
        })
    } else {
        res.status(401).json({
            code: "Unauthorized",
            message: "Invalid session"
        });
    }
}

e.makeXCall = (headers, _puttuKey, _path, _method, _payload) => {
    let _headers = JSON.parse(JSON.stringify(headers));
    return new Promise((_resolve, _reject) => {
        //Get magic key
        e.getUrlandMagicKey(_puttuKey).then(options => {
            //Object.assign(_headers, options.headers);
            _headers.magicKey = options.headers.magicKey;
            _headers.sourceMagicKey = options.headers.sourceMagicKey;
            _headers.masterName = options.headers.masterName;
            options.headers = _headers;

            options.path += _path;
            options.method = _method ? _method.toUpperCase() : 'GET';

            e.makeRequest(options, _payload).then((data) => {
                _resolve(data);
            }).catch((err) => {
                _reject(err);
            })
        }).catch((err) => {
            console.log(err);
            _reject(err);
        });
    })
}

//Generic function to make http request
e.makeRequest = (_options, _payload) => {
    return new Promise(function (resolve, reject) {
        let url = "";
        _options.headers = _options.headers || {};
        delete _options.headers['content-length'];
        // For generate template requests, send url in options
        if (_options.url) {
            url = _options.url
        }
        delete _options["url"];
        if (_payload) {
            _options.headers['content-type'] = 'application/json';
            _options.headers['content-length'] = Buffer.byteLength(JSON.stringify(_payload), 'utf-8');
        }

        // Remove accept-encoding from header for internal call's as it contains compressed data
        if (_options.headers && _options.headers['accept-encoding']) {
            delete _options.headers['accept-encoding'];
        }

        if (url !== "") {
            var req = http.request(url, _options, (res) => {
                res.setEncoding('utf8');
                var data = "";
                res.on("data", _data => data += _data.toString());
                res.on('end', () => {
                    try {
                        data = JSON.parse(data);
                        if (res.statusCode >= 400 && res.statusCode < 600) {
                            if (res.statusCode === 406) reject(data);
                            else reject(data);
                        } else {
                            resolve(data);
                        }
                    } catch (err) {
                        reject(data);
                    }
                });
            });

            req.on('error', (e) => {
                console.error(`problem with request: ${e.message}`);
            });

            // write data to request body
            req.write(JSON.stringify(_payload));
            req.end();
        } else {
            var httpRequest = http.request(_options, response => {
                var data = "";
                response.on("data", _data => data += _data.toString());
                response.on("end", () => {
                    try {
                        data = JSON.parse(data);
                        if (response.statusCode >= 400 && response.statusCode < 600) {
                            if (response.statusCode === 406) reject(data);
                            else reject(data);
                        } else {
                            resolve(data);
                        }
                    } catch (err) {
                        reject(data);
                    }
                });

            });
            if (_payload) {
                httpRequest.write(JSON.stringify(_payload));
            }
            httpRequest.end();
            httpRequest.on("error", err => {
                reject(err);
            });
        }

    });
}

e.setSchemas = function (mongoose) {
    Object.keys(mongoose.models).forEach((item) => {
        puttu.set(item, JSON.stringify(mongoose.modelSchemas[item].obj))
            .then(() => console.log("schema set in redis for: ", item))
            .catch(err => console.log(err));
    });
}

e.sendMail = function (mailObj, headers) {
    var headers = headers ? headers : {};
    return e.makeXCall(headers, 'notification', "/v1/email/send", "POST", mailObj);
}


e.sendSMS = function (smsObj, headers) {
    var headers = headers ? headers : {};
    return e.makeXCall(headers, 'notification', "/v1/sms/send", "POST", smsObj);
}

e.sendPushNotification = function (pushObj, headers) {
    var headers = headers ? headers : {};
    return e.makeXCall(headers, 'notification', "/v1/push/send", "POST", pushObj);
}


e.isPublic = function (url) {
    if (url) {
        var url = url.split("?")[0];
        return publicUrl.indexOf(url) > -1;
    } else {
        return false;
    }
};


/**
 * Function to fetch user's role permission and add in global object for authorization
 */

e.authorize = function (req, res, next) {
    logger.info("Authorized Function Triggered");
    if (req.url && e.isPublic(req.url)) {
        next();
    } else {
        let urlKey = req.url.split("/")[4];
        if (urlKey === "validateToken?api=") {
            urlKey = urlKey.split("?")[0]
        }
        if (path.parse(req.url).base == 'logout') {
            next();
        } else if (path.parse(req.url).base == 'validateToken') {
            next();
        } else {
            if (urlKey && urlKey == 'validateToken') {
                next();
            } else {
                if (req.headers && req.headers.authorization) {
                    var token = req.headers.authorization.split(" ")[1];
                    var userId = jwtDecode(token).user._id;
                    getResourcesViaUserId(userId)
                        .then(resources => {
                            formatResource(resources)
                                .then((gl) => {
                                    global.grantList = gl;
                                    global.roleId = resources.roleIdArray;
                                    next();
                                })
                                .catch((err) => {
                                    res.status(500).send(err);
                                });
                        }).catch((err) => {
                            res.status(500).send(err);
                        });
                } else {
                    next();
                }
            }
        }
    }
}

/**
 * Function to return user's role permission data by UserId 
 */
function getResourcesViaUserId(userId) {
    var roleIds = []
    return new Promise((resolve, reject) => {
        let url = `/v1/${userId}?select=roles`
        e.makeXCall({}, "usermgmt", url)
            .then((data) => {
                if (data && data.roles && data.roles.length !== 0) {
                    data.roles.forEach((x, ind) => {
                        if (x.isDefault) {
                            roleIds = x.roleId;
                        }
                        if (ind == (data.roles.length - 1)) {
                            if (roleIds.length > 0) {
                                const db = mongoose.connection;
                                let filter = {
                                    _id: { $in: roleIds },
                                    deleted: false
                                    // ,status: "Active"
                                }
                                let select = { 'resources': 1 };
                                db.collection('roleMaster').find(filter, select)
                                    .toArray((err, data) => {
                                        if (err) {
                                            logger.error('Error while fetching data from rolemaster collection, Error: ', JSON.stringify(err))
                                            reject({
                                                'statusCode': statusCode.DB_ERROR.statusCode,
                                                'status': statusCode.DB_ERROR.status,
                                                'message': 'Error occurred while fetching role data, RoleIds: ' + roleIds.join(', ')
                                            });
                                        } else {
                                            if (data && data.length !== 0) {
                                                data.roleIdArray = roleIds;
                                                resolve(data);
                                            } else {
                                                reject({
                                                    'statusCode': statusCode.No_data_found.statusCode,
                                                    'status': statusCode.No_data_found.status,
                                                    'message': 'No Role record found'
                                                })
                                            }
                                        }
                                    })
                            } else {
                                reject({
                                    'statusCode': statusCode.Something_went_wrong.statusCode,
                                    'status': statusCode.Something_went_wrong.status,
                                    'message': 'No roles are assigned to the user'
                                })
                            }
                        }
                    });
                } else {
                    reject({
                        'statusCode': statusCode.No_data_found.statusCode,
                        'status': statusCode.No_data_found.status,
                        'message': 'No user record found'
                    })
                }
            })
            .catch(err => {
                reject({
                    'statusCode': statusCode.BAD_REQUEST.statusCode,
                    'status': statusCode.BAD_REQUEST.status,
                    'message': 'No user record found',
                    'error': err
                })
            });
    })
}


/**
 * Format roleMaster data to support accessControl.js library functions
 */
function formatResource(doc) {
    return new Promise((resolve, reject) => {
        var grantList = {};
        async.each(doc, (roleData, cb) => {
            grantList[roleData._id] = {};
            var resourceData = {};

            if(roleData.resources){
                async.each(roleData.resources, (result, innerCb) => {
                    resourceData[result.moduleId] = result.actions;
                    innerCb(null);
                },
                (err) => {
                    if (err) {
                        logger.error(JSON.stringify({
                            error: err
                        }))
                        reject({
                            'statusCode': statusCode.Something_went_wrong.statusCode,
                            'status': statusCode.Something_went_wrong.status,
                            'message': 'Error occured while formating data'
                        })
                    } else {
                        grantList[roleData._id] = resourceData
                        cb()
                    }
                })
            }else{
                cb('Role permission/resources are not defined')
            }

        },
            (err) => {
                if (err) {
                    logger.error(JSON.stringify({
                        error: err
                    }))
                    reject({
                        'statusCode': statusCode.Something_went_wrong.statusCode,
                        'status': statusCode.Something_went_wrong.status,
                        'message': 'Error occured while formating data'
                    })
                } else {
                    resolve(grantList);
                }
            })
    })
}

/**
 * Function to filter data based on permission array
 */
e.authorizationfilter = (keys) => {

    var pathes = keys.reduce((r, path) => {
        var [key, ...rest] = path.split('.'),
            temp = r.find(([q]) => q === key);

        if (rest.length)
            if (temp) temp[1].push(rest.join('.'));
            else r.push([key, [rest.join('.')]]);
        else
            if (!temp) r.push([key]);

        return r;
    }, []);

    return function (object) {
        return Object.fromEntries(pathes.flatMap(([key, rest]) => key in object ? [[
            key,
            rest ? Array.isArray(object[key]) ? object[key].map(e.authorizationfilter(rest)) : e.authorizationfilter(rest)(object[key]) : object[key]
        ]] : []
        ));
    };
}

//Workflow Related Functions
e.getWorkflowDefinition = function (functionName) {
    return function (req, res, next) {
        if (req.query.skipWF && req.query.skipWF === "true") next();
        else {
            console.log('fetching workflow definition :');
            e.makeXCall(req.headers, 'workflowDictionary', "/v1/fetchByEntity/" + encodeURIComponent(functionName)).then((data) => {
                req.workflow = req.workflow || {};
                req.workflow.definition = data;

                next();
            }).catch((err) => {
                res.status(500).json({
                    message: "Workflow: Error occurred while fetching function details"
                });
            });
        }
    }
}

e.getWorkflowRecords = function (functionName) {
    //console.log('functionName is  :', functionName);
    return function (req, res, next) {
        var token = req.headers.authorization.split(" ")[1];
        var userDetail = jwtDecode(token);
        var userId = userDetail.user._id;
        if (req.query && req.query.workflow == 'true') {
            let query = req.query;
            delete query.workflow;
            let filter = query.filter ? JSON.parse(query.filter) : {};
            query.filter = query.filter ? query.filter : {};

            for (let key in filter) {
                if (key.charAt(0) === "$") {
                    //console.log("here1")
                    if (Array.isArray(filter[key])) {
                        // console.log("here2")
                        filter[key] = filter[key].map((item) => {
                            for (let itemKey in item) {
                                item['doc.newDoc.' + itemKey] = item[itemKey];
                                delete item[itemKey];
                            }
                            return item;
                        })
                    } else if (typeof filter[key] === "object") {
                        for (let innerKey in filter[key]) {
                            filter[key]['doc.newDoc.' + innerKey] = filter[key][innerKey];
                            delete filter[key][innerKey]
                        }
                    }
                } else {
                    filter['doc.newDoc.' + key] = filter[key];
                    delete filter[key];
                }
            }

            filter.createdBy = userId;
            filter.type = functionName;

            if (query.workflow_filter) {
                filter.status = JSON.parse(query.workflow_filter);
                delete query.workflow_filter;
            }

            Object.assign(query.filter, {
                "type": functionName,
                "createdBy": userId
            });

            query.filter = encodeURIComponent(JSON.stringify(filter));

            let queryString = '?';

            for (let key in query) {
                if (queryString !== '?') {
                    queryString += '&';
                }
                queryString += `${key}=${query[key]}`;
            }

            req.headers['Cache-Control'] = 'private, no-cache, no-store, must-revalidate';
            res.set('Access-Control-Allow-Origin', '*');

            let ur1 = '/v1/' + queryString;

            e.makeXCall(req.headers, 'workFlow', ur1).then((data) => {
                res.json(data);
            }).catch((err) => {
                console.log(err);
                res.status(500).json(err);
            });
        } else {
            next();
        }
    }
}

e.fetchWorkflowRecord = function (functionName) {
    return function (req, res, next) {
        console.log(functionName)
        if (functionName) {
            req.headers.type = functionName
        } else req.headers.type = '';
        // req.headers.type = functionName || '';
        if (req.query && req.query.workflow == 'true') {
            req.headers['Cache-Control'] = 'private, no-cache, no-store, must-revalidate';
            res.set('Access-Control-Allow-Origin', '*');
            e.makeXCall(req.headers, 'workFlow', '/v1/' + req.params.id).then((rec) => {
                if (rec.status === 'Draft' && rec.createdBy !== userId) {
                    // if(rec.status === 'Draft' && rec.createdBy === "USR1236"){
                    res.status(401).json({
                        message: "You're not authorized to access this workflow record"
                    });
                } else {
                    res.json(rec);
                }
            }).catch((err) => {
                res.status(500).json(err || {
                    message: "Workflow: Error occurred while fetching workflow item details"
                });
            });
        } else {
            next();
        }
    }
}


// kafka producer which will send data/message to the specified User name and IP
e.sendData = function (producer, payloads, params, msg) {
    return new Promise((_resolve, _reject) => {
        try {
            producer.send(payloads, function (err, data) {
                if (!err) {
                    console.log("Message Sent Successfully for Id: " + params.id);
                    _resolve(data);
                } else if (err) {
                    console.log("Message Sent Failed for Id: " + params.id);
                    _reject({ statusCode: 101, meesage: err });
                }
            });
        } catch (e) {
            _reject(e);
        }
    });
}


module.exports = e;